// Waiting load DOM active tab and register event click icon extension
chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if (changeInfo.status == 'complete') {
        chrome.browserAction.onClicked.addListener(function () {
            chrome.tabs.query({active: true, status: "complete", windowType: "normal"}, function (tabs) {

                if (tabs && tabs[0] && tabs[0].url && !excludePath(tabs[0].url)) {
                    chrome.cookies.get({"url": tabs[0].url, "name": "dev"}, function (cookie) {
                        if (cookie && cookie.value === "true") {
                            chrome.cookies.remove({url: tabs[0].url, name: "dev"}, function () {
                                chrome.browserAction.setBadgeText({text: 'off'});
                                chrome.browserAction.setBadgeBackgroundColor({color: "#d32f2f"});

                                chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
                                    chrome.tabs.update(tabs[0].id, {url: tabs[0].url});
                                });
                            });
                        } else {
                            chrome.cookies.set({url: tabs[0].url, name: "dev", value: "true"}, function () {
                                chrome.browserAction.setBadgeText({text: 'on'});
                                chrome.browserAction.setBadgeBackgroundColor({color: "#388E3C"});

                                chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
                                    chrome.tabs.update(tabs[0].id, {url: tabs[0].url});
                                });
                            });
                        }
                    });
                }

            });
        });
    }
});

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
    if (changeInfo.status == 'complete') {
        chrome.tabs.query({active: true, status: "complete", windowType: "normal"}, function (tabs) {

            if (tabs && tabs[0] && tabs[0].url && !excludePath(tabs[0].url)) {
                chrome.cookies.get({"url": tabs[0].url, "name": "dev"}, function (cookie) {
                    if (cookie && cookie.value === "true") {
                        chrome.browserAction.setBadgeText({text: 'on'});
                        chrome.browserAction.setBadgeBackgroundColor({color: "#388E3C"});
                    }
                });
            }

        });
    }
});

function excludePath(url) {
    let list = ["chrome-devtools:", "chrome:", "browser:", "chrome-extension:"];

    let i = list.length;

    for (i; i--;) {
        console.log(url);
        console.log(list[i]);
        if (url.indexOf(list[i]) >= 0) return true;
    }

    return false;
}