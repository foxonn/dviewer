/* global chrome */

searchDump = (function(){
    let remove_content = [];
    let dump_list = Array();

    return {
        searchDumpContent:function(tag = 'script',attr = 'type',value = 'source/dump'){

            let dump = document.getElementsByTagName(tag);

            if(dump.length > 0){
                let i = dump.length;
                for(i; i--;){
                    if(dump[i].hasAttribute(attr) && dump[i].getAttribute(attr) === value){

                        let array = Array();

                        if(dump[i].textContent){
                            if(dump[i].textContent !== ''){
                                array.push({content:dump[i].textContent});
                            }else{
                                continue;
                            }
                        }

                        if(dump[i].hasAttribute('line')){
                            if(dump[i].getAttribute('line') !== ''){
                                array.push({line:dump[i].getAttribute('line')});
                            }
                        }

                        if(dump[i].hasAttribute('name')){
                            if(dump[i].getAttribute('name') !== ''){
                                array.push({name:dump[i].getAttribute('name')});
                            }
                        }

                        if(dump[i].hasAttribute('comment')){
                            if(dump[i].getAttribute('comment') !== ''){
                                array.push({comment:dump[i].getAttribute('comment')});
                            }
                        }

                        if(dump[i].hasAttribute('params')){
                            if(dump[i].getAttribute('params') !== ''){
                                array.push({params:dump[i].getAttribute('params')});
                            }
                        }

                        dump_list.push(array);

                        remove_content.push(dump[i]);
                    }
                }
            }

        },
        removeDumpDom:function(){
            let i = remove_content.length;

            for(i; i--;){
                remove_content[i].remove();
            }
        },
        getDumpList:function(){
            return dump_list;
        }
    }
}());

searchDump.searchDumpContent();
searchDump.removeDumpDom();

Message = (function(chrome){
    return {
        _sendMessage:function(params){
            chrome.runtime.sendMessage(params);
        },
        _listenMessage:function(callback,sender_name){
            chrome.runtime.onMessage.addListener(function(request){
                if(request.sender === sender_name){
                    callback(request);
                }
                return true;
            });
        }
    }
}(chrome));

//load document
window.addEventListener("load",function(){
    Message._sendMessage({
        sender:"content",
        type:"dom-loaded",
        data:{page:JSON.stringify(searchDump.getDumpList())}
    });
},true);