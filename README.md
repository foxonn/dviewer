#####Этот WebExtension DViewer предназначен, для вывода сообщений отдельно во вкладке devtools.

#####Обычный режим

**Атрибуты** `name, line, comment` **устанавливаются опционально**

```
echo '<script type="source/dump" name="info" line="'.dirname(__FILE__)./".basename(__FILE__)." Line: ".__LINE__.' comment="My comment!!!">';
    var_dump($_SERVER);
echo '</script>';
```

#####Скрытый режим если установлен `cookie "dev=true"`

```
if($_COOKIE['dev']) {
echo '<script type="source/dump" name="info" line="'.dirname(__FILE__)./".basename(__FILE__)." Line: ".__LINE__.' comment="My comment!!!">';
    var_dump($_SERVER);
echo '</script>';
}
```

`cookie "dev=true"`

![Отображение вкладки DViewer](DViewer-cookie-on.png)


`cookie ""`

![Отображение вкладки DViewer](DViewer-cookie-off.png)

####Пример

![Отображение вкладки DViewer](DViewer-example.png)


![Отображение вкладки DViewer](DViewer-example-2.png)

