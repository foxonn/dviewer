Search = (function(){
    return {
        findSubString:function(string){

            function domRangeHighlight(text){
                // Получим текстовый узел
                var root = document.getElementById('0').getElementsByClassName('xdebug-var-dump')[0];
                // Проверим есть ли совпадения с переданным текстом
                if(root.innerText.indexOf(text)){
                    if(root.createTextRange){
                        let range = root.createTextRange();
                        range.moveToElementText(text);
                        range.select();
                    }
                }
            }

            domRangeHighlight(string);
        }
    }
}());

dumpPanel = (function(){
    let dump_content;
    let dump_list;
    let var_dump_block;
    let tool_bar;
    let tool_bar_btn;

    return {
        setDumpContent:function(dump){
            dump_content = dump;
        },
        getDumpContent:function(){
            return dump_content;
        },
        getDumpList:function(){
            return dump_list;
        },
        addWrapperDump:function(){
            var_dump_block = document.createElement('div');
            var_dump_block.className = "php-var-dump";

            document.body.innerHTML = '';
            document.body.appendChild(var_dump_block);
        },
        appendDumpBlock:function(content = ''){
            dump_list = document.createElement('ul');

            dump_list.innerHTML = content;
            dump_list.className = 'view-dump';

            var_dump_block.appendChild(dump_list);
        },
        appendDumpList:function(){
            let count = dump_content.length;

            for(count; count--;){
                let row_dump = document.createElement('li');
                let block_name;
                let block_line;
                let block_content;
                let block_order;
                let block_comment;

                if(dump_content[count][0]['content']){
                    let content = dump_content[count][0]['content'];
                    block_content = document.createElement('pre');
                    block_content.innerHTML = content;
                    block_content.className = 'dump-content';
                    block_content.setAttribute('id','' + count);
                }else{
                    continue;
                }

                if(dump_content[count][1] && dump_content[count][1]['line']){
                    let line = dump_content[count][1]['line'];
                    block_line = document.createElement('div');
                    block_line.innerHTML = 'Address: ' + line;
                }

                if(dump_content[count][3] && dump_content[count][3]['comment']){
                    let name = dump_content[count][3]['comment'];
                    block_comment = document.createElement('div');
                    block_comment.innerHTML = 'Comment: ' + name;
                }

                if(dump_content[count][1] && dump_content[count][2]['name']){
                    let name = dump_content[count][2]['name'];
                    block_name = document.createElement('div');
                    block_name.innerHTML = 'Name: ' + name;
                    row_dump.setAttribute('class',name)
                }

                block_order = document.createElement('div');
                block_order.innerHTML = 'Order: ' + count;

                if(block_name || block_line){
                    let row_info = document.createElement('div');
                    row_info.className = 'dump-info';

                    if(block_name) row_info.appendChild(block_name);
                    if(block_line) row_info.appendChild(block_line);
                    if(block_comment) row_info.appendChild(block_comment);
                    if(block_order) row_info.appendChild(block_order);

                    row_dump.appendChild(row_info);
                }

                if(block_content){
                    row_dump.appendChild(block_content);
                    dumpPanel.addOpenFullPage(block_content);
                    dumpPanel.addWordWrap(block_content);
                }

                dump_list.appendChild(row_dump);
            }

        },
        addToolBar:function(){
            tool_bar = document.createElement('div');
            tool_bar.className = "tool-bar";

            var_dump_block.appendChild(tool_bar);
        },
        addToolBarButtons:function(){
            tool_bar_btn = document.createElement('ul');
            tool_bar_btn.className = "tool-bar-buttons";

            tool_bar.appendChild(tool_bar_btn);
        },
        appendToolBarButtons:function(){
            let count = dump_content.length;
            let name;

            for(count; count--;){
                if(typeof dump_content[count][2]['name'] === 'undefined') continue;

                if(name = dump_content[count][2]['name']){
                    let button;

                    if(tool_bar.getElementsByClassName(name).length){
                        button = tool_bar.getElementsByClassName(name);
                        let total = button[0].getAttribute('total');
                        button[0].setAttribute('total','' + (+total + 1));
                        button[0].innerHTML = name + ' (' + (+total + 1) + ')';
                        continue;
                    }

                    button = document.createElement('li');
                    button.setAttribute('total','' + 1);
                    button.className = name;
                    button.innerHTML = name;

                    button.addEventListener('click',function(elem){
                        let _class = elem.target.getAttribute('class');

                        if(_class.split(' ').length > 1){
                            _class = _class.split(' ')[0];

                            elem.target.setAttribute('class',_class);

                            let select_name = dump_list.getElementsByClassName(_class);
                            let i = select_name.length;
                            for(i; i--;){
                                select_name[i].style.display = 'block';
                            }
                        }else{
                            elem.target.setAttribute('class',_class + ' active');

                            let select_name = dump_list.getElementsByClassName(_class);
                            let i = select_name.length;
                            for(i; i--;){
                                select_name[i].style.display = 'none';
                            }
                        }

                    });

                    tool_bar_btn.appendChild(button);
                }
            }

        },
        addSearchPanel:function(){
            let search_panel = document.createElement('div');
            search_panel.setAttribute('class','search-panel');

            let search_input = document.createElement('input');
            search_input.setAttribute('class','search-input');

            search_input.onkeypress = function(elem){
                if(elem.target.value){
                    Search.findSubString(elem.target.value);
                }
            };

            search_panel.appendChild(search_input);
            tool_bar.appendChild(search_panel);
        },
        addOpenFullPage:function(element){
            let full_page = document.createElement('div');

            full_page.className = "btn open-full-page";
            full_page.setAttribute("title","Open in Window");

            full_page.addEventListener('click',function(elem){
                let parent = elem.target.parentNode;

                let p_class = parent.getAttribute('class');

                if(p_class.indexOf('full-page') >= 0){
                    p_class = p_class.replace("full-page",'').trim();
                    parent.setAttribute('class',p_class);
                    dumpPanel.overflowBody();
                }else{
                    p_class += " full-page";
                    parent.setAttribute('class',p_class);
                    dumpPanel.overflowBody();
                }

                let e_class = elem.target.getAttribute('class');

                if(e_class.indexOf('active') >= 0){
                    e_class = e_class.replace("active",'').trim();
                    elem.target.setAttribute('class',e_class);
                }else{
                    e_class += " active";
                    elem.target.setAttribute('class',e_class);
                }

            });

            element.appendChild(full_page);
        },
        addWordWrap:function(element){
            let word_wrap = document.createElement('div');
            word_wrap.className = "btn word-wrap";
            word_wrap.setAttribute("title","Word wrap");

            word_wrap.addEventListener('click',function(elem){
                let parent;

                parent = elem.target.parentNode;

                if(elem.target.parentNode.querySelector('.xdebug-var-dump')) {
                    parent = elem.target.parentNode.querySelector('.xdebug-var-dump');
                }

                let p_class = parent.getAttribute('class');

                if(p_class.indexOf('break') >= 0){
                    p_class = p_class.replace("break",'').trim();
                    parent.setAttribute('class',p_class);
                }else{
                    p_class += " break";
                    parent.setAttribute('class',p_class);
                }

                let e_class = elem.target.getAttribute('class');

                if(e_class.indexOf('active') >= 0){
                    e_class = e_class.replace("active",'').trim();
                    elem.target.setAttribute('class',e_class);
                }else{
                    e_class += " active";
                    elem.target.setAttribute('class',e_class);
                }
            });

            element.appendChild(word_wrap);
        },
        overflowBody: function(){
            let parent = document.body;
            let p_class;

            if(parent.getAttribute('class')) {
                p_class = parent.getAttribute('class');

                if(p_class.indexOf('hidden') >= 0){
                    p_class = p_class.replace("hidden",'').trim();
                    parent.setAttribute('class',p_class);
                }else{
                    p_class += " full-page";
                    parent.setAttribute('class',p_class);
                }
            }else{
                parent.setAttribute('class', 'hidden');
            }
        }
    }
}());

Message = (function(chrome){
    return {
        _sendMessage:function(params){
            chrome.runtime.sendMessage(params);
        },
        _listenMessage:function(callback,sender_name){
            chrome.runtime.onMessage.addListener(function(request){
                if(request.sender === sender_name){
                    callback(request);
                }
                return true;
            });
        }
    }
}(chrome));

Message._listenMessage(func = function(request){
    if(request.type == "dom-loaded"){
        let dump = JSON.parse(request.data.page);

        if(dump.length){
            dumpPanel.setDumpContent(dump);
            dumpPanel.addWrapperDump();
            dumpPanel.addToolBar();
            dumpPanel.addToolBarButtons();
            dumpPanel.appendToolBarButtons();
            // dumpPanel.addSearchPanel();
            dumpPanel.appendDumpBlock();
            dumpPanel.appendDumpList();
        }
    }

},"content");